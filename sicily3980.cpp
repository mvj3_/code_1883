//题目分析: NULL

//题目网址:http://soj.me/3980

#include<iostream>
#include<string>
#include<cmath>
using namespace std;
int main()
{
    int t;
    cin >> t;
    string bin;
    while(t--){
        cin >> bin;
        int i;
        int dec = 0;
        int len = bin.length();
        for(i = 0; i < len; i++){
            dec += int((bin[i] - '0') * pow(double(2),len - 1 - i));
        }
        cout << dec << endl;
    }
    return 0;
}                                 